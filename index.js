
// đầu vào: số ngày làm việc
// các bước xử lý: lấy số ngày làm việc nhân với tiền lương 1 ngày là 100000
// đầu ra: lương
console.log("Bai 1: ");
const paymentPerDay = 100000;
var day = 6;
console.log("So ngay lam viec: ", day);
console.log("Salary: ", paymentPerDay * day);

// đầu vào: 5 số thực
// các bước xử lý: lấy tổng 5 số rồi chia cho 5
// đầu ra: trung bình cộng 5 số
console.log("Bai 2: ");
var a = 1;
var b = 2;
var c = 3;
var d = 4;
var e = 5;
console.log("Average value: ", (a + b + c + d + e) / 5);

// đầu vào: số đồng USD cần quy đổi
// các bước xử lý: lấy số đồng USD nhân với 23500
// đầu ra: giá trị đồng VND sau khi quy đổi
console.log("Bai 3: ");
var USD = 8;
console.log("USD: ", USD);
console.log(USD + " USD quy doi ra VND la: ", USD * 23500);

// đầu vào: chiều dài, chiều rộng HCN
// các bước xử lý: 
// Diện tích: lấy chiều dài nhân với chiều rộng
// Chu vi: lấy tổng chiều dài và chiều rộng rồi nhân 2
// đầu ra: diện tích và chu vi HCN
console.log("Bai 4: ");
var long = 10;
var width = 5;
console.log("Chieu dai cua HCN la: ", long);
console.log("Chieu rong cua HCN la: ", width);
console.log("Dien tich cua HCN la: ", long * width);
console.log("Chu vi cua HCN la: ", (long + width) * 2);

// đầu vào: 1 số có 2 chữ số
// các bước xử lý: 
// hàng đơn vị: lấy số ban đầu chia 10 lấy dư
// hàng chục: lấy số ban đầu trư số ahfng đơn vị xong chia 10
// Sau đó cộng 2 chữ số hàng chục và hàng đơn vị
// đầu ra: tổng của 2 ký số
console.log("Bai 5: ");
var number = 26;
var units = number % 10;
var dozens = (number - units) / 10;
console.log("Number: ", number);
console.log("Tong 2 ky so la: ", dozens + units);